import urllib3
import json
import os

http = urllib3.PoolManager()


def lambda_handler(event, context):
    url = os.environ["slack_webhook_url"]
    data = event["Records"][0]
    timestamp = data["Sns"]["Timestamp"]
    timestamp = timestamp.replace("T", " ").replace("Z", "")
    task_details = json.loads(data["Sns"]["Message"])
    if "taskName" in task_details:
        failure_type_name = task_details["taskName"]
        error_meesage = task_details["messages"][0]["errorMessage"]
        title = "Snowflake Task Failure Alert"
    if "pipeName" in task_details:
        failure_type_name = task_details["pipeName"]
        error_meesage = task_details["messages"][0]["firstError"]
        title = "Snowpipe Failure Alert"

    error_message_markdown = f"```{error_meesage}```"

    log_link = "https://gitlab.com/gitlab-data/runbooks/-/blob/main/triage_issues/snowflake_pipeline_failure_triage.md"
    log_link_markdown = f"<{log_link}|Runbook>"

    message = {
        "channel": "#data-pipelines",
        "username": "SNOWFLAKE_TASK_PIPE",
        "icon_emoji": "snowflake",
        "attachments": [
            {
                "fallback": "Snowflake Task Failure Alert",
                "color": "#FF0000",
                "fields": [
                    {"title": title, "value": failure_type_name},
                    {"title": "Timestamp", "value": timestamp},
                    {"title": "Error Message", "value": error_message_markdown},
                    {
                        "title": "Steps to view error message in snowflake",
                        "value": log_link_markdown,
                    },
                ],
            }
        ],
    }

    encoded_msg = json.dumps(message).encode("utf-8")
    resp = http.request("POST", url, body=encoded_msg)

    print(
        {
            # "message": event['Records'],
            "status_code": resp.status,
            "response": resp.data,
        }
    )
