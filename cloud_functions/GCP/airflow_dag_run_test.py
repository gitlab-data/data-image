import os
import pg8000
import sqlalchemy
import requests
import json


connection_name = os.environ["INSTANCE_CONNECTION_NAME"]
db_name = os.environ["DB_NAME"]
db_user = os.environ["DB_USER"]
db_password = os.environ["DB_PASS"]
webhook = os.environ["WEB_HOOK"]


# If your database is PostgreSQL, uncomment the following two lines:
driver_name = "postgres+pg8000"
query_string = dict({"unix_sock": "/cloudsql/{}/.s.PGSQL.5432".format(connection_name)})

# If the type of your table_field value is a string, surround it with double quotes.


def send_slack_message(payload):
    """Send a Slack message to a channel via a webhook."""
    print(payload)
    from slack_sdk.webhook import WebhookClient

    webhook_send = WebhookClient(webhook)

    webhook_send.send(
        text=f"Long running Task Alert:{json.dumps(payload, indent=4, sort_keys=False, default=str)}"
    )


def validate_dag(list_dag):
    pay_load = []
    for dag in list_dag:
        if dag[0] == "dbt" and dag[3] > 5:
            dag_payload = {
                "dag_name": dag[0],
                "task_name": dag[1],
                "dag_start_time": dag[2],
                "dag_running_for_hours": dag[3],
            }
            pay_load.append(dag_payload)
        else:
            dag_payload = {
                "dag_name": dag[0],
                "task_name": dag[1],
                "dag_start_time": dag[2],
                "dag_running_for_hours": dag[3],
            }
            pay_load.append(dag_payload)
    print(pay_load)
    if pay_load != []:
        send_slack_message(pay_load)


def get_dag_status(request):
    request_json = request.get_json()
    stmt = sqlalchemy.text(
        "SELECT dag_id,task_id, start_date::timestamp,\
                            ROUND(((EXTRACT(EPOCH FROM current_timestamp) - EXTRACT(EPOCH FROM start_date))/3600)::numeric,3) as duration_of_task_run_time \
                            FROM task_instance WHERE state = 'running'  and (EXTRACT(EPOCH FROM current_timestamp) - EXTRACT(EPOCH FROM start_date))/3600 > 4 \
                            AND dag_id not in ('saas_usage_ping');"
    )

    db = sqlalchemy.create_engine(
        sqlalchemy.engine.url.URL(
            drivername=driver_name,
            username=db_user,
            password=db_password,
            database=db_name,
            query=query_string,
        ),
        pool_size=5,
        max_overflow=2,
        pool_timeout=30,
        pool_recycle=1800,
    )
    try:
        with db.connect() as conn:
            list_dag = conn.execute(stmt)
            validate_dag(list_dag.fetchall())
    except Exception as e:
        return "Error: {}".format(str(e))
    return "ok"
